<?php
/**
 * Created by PhpStorm.
 * User: dev-bassem
 * Date: 06/12/18
 * Time: 19:12
 */

namespace AppBundle\Form;

use AppBundle\Entity\PropertySearch;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class PropertySearchType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('minsurface',IntegerType::class,[
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Surface minimal'
                ]
            ])
            ->add('maxprice',IntegerType::class,[
            'required' => false,
            'label' => false,
            'attr' => [
                'placeholder' => 'Budget max'
            ]
            ])
            ->add('ville',null,[
            'required' => false,
            'label' => false,
            'attr' => [
                'placeholder' => 'ville'
            ]
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PropertySearch',
            'method' => 'get',
            'csrf_protection' => false
        ));
    }


    public function getBlockPrefix()
    {
        return '';
    }
}