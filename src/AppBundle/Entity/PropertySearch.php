<?php
/**
 * Created by PhpStorm.
 * User: dev-bassem
 * Date: 06/12/18
 * Time: 18:24
 */

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;


class PropertySearch
{





    /**
     * @var string|null
     */
    private  $ville;

    /**
     * @var int|null
     */
    private  $maxprice;


    /**
     * @var int|null
     * @Assert\Range(
     *      min = 10,
     *      max = 400,
     *      minMessage = "You must be at least {{ limit }}m tall to enter",
     *      maxMessage = "You cannot be taller than {{ limit }}m to enter"
     * )
     */
    private $minsurface;

    /**
     * @return int|null
     */
    public function getMaxprice()
    {
        return $this->maxprice;
    }

    /**
     * @param int|null $maxprice
     */
    public function setMaxprice(int $maxprice)
    {
        $this->maxprice = $maxprice;
    }

    /**
     * @return int|null
     */
    public function getMinsurface()
    {
        return $this->minsurface;
    }

    /**
     * @param int|null $minsurface
     */
    public function setMinsurface( int $minsurface)
    {
        $this->minsurface = $minsurface;
    }

    /**
     * @return null|string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param null|string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }



}