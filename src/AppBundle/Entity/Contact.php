<?php
/**
 * Created by PhpStorm.
 * User: dev-bassem
 * Date: 06/12/18
 * Time: 21:39
 */

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;


class Contact
{
    /**
     * @var String|null
     * @Assert\NotBlank()
     * @Assert\Length(min="2",max="100")
     */
    private $nom;


    /**
     * @var String|null
     * @Assert\NotBlank()
     * @Assert\Length(min="2",max="100")
     */
    private $prenom;
    /**
     * @var String|null
     * @Assert\NotBlank()
     */
    private $tel;
    /**
     * @var String|null
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;
    /**
     * @var String|null
     * @Assert\NotBlank()
     * @Assert\Length(min="10")
     */
    private $message;


    /**
     * @var Property|null
     */

    private $property;

    /**
     * @return null|String
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param null|String $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return null|String
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param null|String $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return null|String
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * @param null|String $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * @return null|String
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param null|String $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return null|String
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param null|String $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return Property|null
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param Property|null $property
     */
    public function setProperty($property)
    {
        $this->property = $property;
    }



}