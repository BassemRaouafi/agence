<?php
/**
 * Created by PhpStorm.
 * User: dev-bassem
 * Date: 06/12/18
 * Time: 22:58
 */

namespace AppBundle\Notification;


use AppBundle\Entity\Contact;
use Twig\Environment;

class ContactNotification
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var Environment
     */
    private $render;

    public function __construct(\Swift_Mailer $mailer, Environment $render)
    {

        $this->mailer = $mailer;
        $this->render = $render;
    }

    /**
     * @param Contact $contact
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function notify(Contact $contact)
    {



        $message = (new \Swift_Message('Hello Email'))
            ->setSubject('Sujet Test')
            ->setFrom('raouafi.bassem06@gmail.com')
            ->setTo('raouafi.bassem06@gmail.com')
            ->setBody("plopppp mail" );


        $this->mailer->send($message);

     /*$message = (new \Swift_Mailer('Agence : '.$contact->getProperty()->getTitle()))
    ->setFrom('raouafi.bassem06@gmail.com')
    ->setTo('raouafi.bassem06@gmail.com')
    ->setReplyTo($contact->getEmail())
    ->setBody($this>$this->render->render('emails/contact.html.twig',[
        'contact' =>$contact
        ]),'text/html');

$this->mailer->send($message);*/
    }

}