<?php

namespace AppBundle\Controller\admin;



use AppBundle\Entity\Property;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Property controller.
 *
 * @Route("/admin")
 */

class AdminPropertyController extends AbstractController
{




    /**
     * @Route("/",name="adminbassem_property_index")
     * @return Response
     */
    public function index()
    {

        $em = $this->getDoctrine()->getManager();

        $properties = $em->getRepository('AppBundle:Property')->findAllOrderedByTitle();

        return $this->render('admin/dashboard/test.html.twig',[
            'properties' => $properties,

        ]);
    }

    /**
     * @Route("/{id}",name="adminbassem_property_edit")
     * @param Property $property
     * @param Request $request
     * @return Response
     */
    public function edit(Property $property,Request $request)
    {

        $form = $this->createForm('AppBundle\Form\PropertyType', $property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          $this->getDoctrine()->getManager()->flush();
          $this->addFlash('success','Bien modifié avec succès');
          return $this->redirectToRoute('adminbassem_property_index', array('id' => $property->getId()));
           

        }
        return $this->render('admin/dashboard/propertyShow.html.twig'
            ,[
                'property' => $property,
                'form' => $form->createView(),
            ]);
    }

    /**
     * Creates a new property entity.
     *
     * @Route("/new", name="adminbassem_property_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $property = new Property();
        $form = $this->createForm('AppBundle\Form\PropertyType', $property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($property);
            $em->flush();

            return $this->redirectToRoute('adminbassem_property_show', array('id' => $property->getId()));
        }

        return $this->render('admin/new.html.twig', array(
            'property' => $property,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a property entity.
     *
     * @Route("/{id}", name="adminbassem_property_show")
     * @Method("GET")
     * @param Property $property
     * @return Response
     */
    public function showAction(Property $property)
    {
        $deleteForm = $this->createDeleteForm($property);

        return $this->render('admin/dashboard/propertyShow.html.twig', array(
            'property' => $property,
            'delete_form' => $deleteForm->createView(),
        ));
    }




    /**
     * Creates a form to delete a property entity.
     *
     * @param Property $property The property entity
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Property $property)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_property_delete', array('id' => $property->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
