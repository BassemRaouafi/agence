<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use AppBundle\Entity\Property;
use AppBundle\Entity\PropertySearch;
use AppBundle\Form\PropertySearchType;
use AppBundle\Notification\ContactNotification;
use AppBundle\Repository\PropertyRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class PropertyController extends Controller
{




    /**
     * Lists all property entities.
     *
     * @Route("/", name="home")
     * @Method("GET")
     */
    public function indexAction(PaginatorInterface $paginator, Request $request)
    {

        $search = new PropertySearch();
        $em = $this->getDoctrine()->getManager();

        $properties = $paginator->paginate(
            $em->getRepository('AppBundle:Property')->findAllVisibleQuery($search),
            $request->query->getInt('page',1),12);

        return $this->render('pages/index.html.twig', array(
            'properties' => $properties,

        ));
    }



    /**
     * Lists all property entities.
     *
     * @Route("/lesbiens", name="property_list")
     * @Method("GET")
     */
    public function listPropertyAction(PaginatorInterface $paginator, Request $request)
    {
        $search = new PropertySearch();
        $form = $this->createForm('AppBundle\Form\PropertySearchType', $search);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $properties = $paginator->paginate(
            $em->getRepository('AppBundle:Property')->findAllVisibleQuery($search),
            $request->query->getInt('page',1),12);

        return $this->render('pages/propertyList.html.twig', array(
            'properties' => $properties,
            'form' => $form->createView()
        ));
    }

    /**
     * Creates a new property entity.
     *
     * @Route("/new", name="admin_property_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $property = new Property();
        $form = $this->createForm('AppBundle\Form\PropertyType', $property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($property);
            $em->flush();

            return $this->redirectToRoute('admin_property_show', array('id' => $property->getId()));
        }

        return $this->render('admin/property/new.html.twig', array(
            'property' => $property,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a property entity.
     *
     * @Route("/biens/{slug}-{id}", name="property_show", requirements={"slug": "[a-z0-9\-]*"})
     * @param Property $property
     * @param string $slug
     * @param Request $request
     * @param ContactNotification $notification
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function showAction(Property $property,string $slug,Request $request,ContactNotification $notification)
    {


       if($property->getSlug() !== $slug)
       {
          return $this->redirectToRoute('property_show', [
              'id' => $property->getId(),
              'slug' => $property->getSlug()
           ],301);
       }

        $contact = new Contact();
        $contact->setProperty($property);
        $form =$this->createForm('AppBundle\Form\ContactType',$contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
$notification->notify($contact);
            $this->addFlash('success','votre email a bien été envoyé');
            return $this->redirectToRoute('property_show', [
                'id' => $property->getId(),
                'slug' => $property->getSlug()
            ]);
        }


        return $this->render('pages/show.html.twig', array(
            'property' => $property,
            'form' => $form->createView(),

        ));
    }

    /**
     * Displays a form to edit an existing property entity.
     *
     * @Route("/{id}/edit", name="admin_property_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Property $property
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Property $property)
    {
        $deleteForm = $this->createDeleteForm($property);
        $editForm = $this->createForm('AppBundle\Form\PropertyType', $property);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_property_edit', array('id' => $property->getId()));
        }

        return $this->render('admin/property/edit.html.twig', array(
            'property' => $property,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a property entity.
     *
     * @Route("/{id}", name="admin_property_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Property $property)
    {
        $form = $this->createDeleteForm($property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($property);
            $em->flush();
        }

        return $this->redirectToRoute('admin_property_index');
    }

    /**
     * Creates a form to delete a property entity.
     *
     * @param Property $property The property entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Property $property)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_property_delete', array('id' => $property->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
