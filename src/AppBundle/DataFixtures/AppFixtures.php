<?php
/**
 * Created by PhpStorm.
 * User: dev-bassem
 * Date: 03/12/18
 * Time: 23:08
 */
namespace AppBundle\DataFixtures;
use AppBundle\Entity\Property;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 20; $i++) {
            $property = new Property();
            $property
                ->setTitle($faker->words(3,true))
                ->setDescription($faker->sentence(3,true))
                ->setSurface($faker->numberBetween(20,350))
                ->setRooms($faker->numberBetween(2,10))
                ->setBedrooms($faker->numberBetween(1,9))
                ->setFloor($faker->numberBetween(0,15))
                ->setPrice($faker->numberBetween(100000,1000000))
                ->setHeat($faker->numberBetween(0,count(Property::HEAT) - 1 ))
                ->setCity($faker->city)
                ->setAddress($faker->address)
                ->setPostalcode($faker->postcode)
                ->setSold(false);
            $manager->persist($property);
        }

        $manager->flush();
    }
}